﻿Shader "sonil/shadow_color"
{
	SubShader
	{
		Pass
		{
			Tags { "RenderPipeline" = "shadow_color" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 uv : TEXCOORD0;
			};

			sampler2D _ScreenSpaceShadowMap;
			sampler2D _CameraDepth;
			float4x4 _m;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv.xy = v.uv;
				o.uv.zw = ((o.pos.xy / o.pos.w) + 1) / 2;
				return o;
			}
			
			fixed4 world_color(float3 wpos)
			{
				if(wpos.y > 0) {
					if(wpos.x > 0)
						return fixed4(1,0,0,0);
					return fixed4(0,1,0,0);
				}
				return fixed4(0,0,1,0);
			}

			fixed4 cal_wpos(float3 wpos)
			{
				float4 spos = mul(_m,float4(wpos.xyz,1));

				float3 ndc = spos.xyz / spos.w;
				ndc = (ndc + 1) / 2;
				ndc.z = 1-ndc.z;

				if(ndc.x > 1 || ndc.x < 0 || ndc.y > 1 || ndc.y < 0)
					return fixed4(0,0,0,0);

				float4 tmp = tex2D(_ScreenSpaceShadowMap,ndc.xy);
				float depth = tmp.z / tmp.w;

				if(ndc.z > depth-0.005)
					return fixed4(0,0,0,0);
				return fixed4(0.5,0.5,0.5,0);
			}

			fixed4 frag (v2f i) : SV_Target
			{
				float4 tmp = tex2D(_CameraDepth,i.uv);
				if(tmp.z==0 || tmp.z == 0)
				{
					return fixed4(0,0,0,0);
				}
	
				float depth = 1 - (tmp.z / tmp.w);

				float4 cpos = float4(i.uv.zw * 2 - 1, depth * 2 - 1, 1.0);
				cpos.y = -cpos.y;

				float4 vpos = mul(unity_CameraInvProjection, cpos);//转到camera view
				vpos.z = -vpos.z;//camera view为右手坐标系
				vpos = vpos / vpos.w;

				float4 wpos = mul(unity_CameraToWorld,float4(vpos.xyz,1));//转到world

				//return world_color(wpos.xyz);
				return cal_wpos(wpos.xyz);
			}
			ENDCG
		}
	}
}

